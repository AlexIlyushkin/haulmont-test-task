package com.haulmont.testtask;

import com.haulmont.testtask.persistance.JpaConfiguration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import org.vaadin.spring.events.annotation.EnableEventBus;
import org.vaadin.spring.events.config.EventBusConfiguration;

/**
 * @author Alex Ilyushkin
 */
@SpringBootApplication
@Import({ JpaConfiguration.class, EventBusConfiguration.class})
@EnableEventBus
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
