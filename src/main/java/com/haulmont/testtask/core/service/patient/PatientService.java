package com.haulmont.testtask.core.service.patient;

import com.haulmont.testtask.persistance.entity.Patient;
import com.haulmont.testtask.core.service.CrudService;

/**
 * @author Alex Ilyushkin
 */
public interface PatientService extends CrudService<Patient> {
}
