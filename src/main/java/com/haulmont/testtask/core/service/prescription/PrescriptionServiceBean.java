package com.haulmont.testtask.core.service.prescription;

import com.haulmont.testtask.core.component.PrescriptionSearchQueryBuilder;
import com.haulmont.testtask.persistance.dao.PrescriptionDao;
import com.haulmont.testtask.persistance.entity.Doctor;
import com.haulmont.testtask.persistance.entity.Patient;
import com.haulmont.testtask.persistance.entity.Prescription;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * @author Alex Ilyushkin
 */
@Service("prescriptionService")
@Transactional
public class PrescriptionServiceBean implements PrescriptionService {

    @Resource(name = "prescriptionDao")
    private PrescriptionDao prescriptionDao;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private PrescriptionSearchQueryBuilder prescriptionSearchQueryBuilder;

    @Override
    public Prescription save(Prescription prescription) {
        return prescriptionDao.save(prescription);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Prescription> findAll() {
        return prescriptionDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Prescription> findAllWithDoctors() {
        return prescriptionDao.findAllWithDoctors();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Prescription> findAllWithPatients() {
        return prescriptionDao.findAllWithPatients();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Prescription> findAllWithDoctorsAndPatients() {
        return prescriptionDao.findAllWithDoctorsAndPatients();
    }

    @Override
    public List<Prescription> findPrescriptionBy(Doctor doctor,
                                                 Patient patient,
                                                 String description,
                                                 Prescription.Priority priority) {
        CriteriaQuery<Prescription> cq = prescriptionSearchQueryBuilder.buildSearchQuery(
                em.getCriteriaBuilder(), doctor, patient, description, priority);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public void delete(Prescription prescription) {
        prescriptionDao.delete(prescription);
    }
}
