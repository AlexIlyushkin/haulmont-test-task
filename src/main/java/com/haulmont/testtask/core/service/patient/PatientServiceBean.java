package com.haulmont.testtask.core.service.patient;

import com.haulmont.testtask.persistance.dao.PatientDao;
import com.haulmont.testtask.persistance.entity.Patient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Alex Ilyushkin
 */
@Service("patientService")
@Transactional
public class PatientServiceBean implements PatientService {

    @Resource(name = "patientDao")
    private PatientDao patientDao;

    @Override
    public Patient save(Patient patient) {
        return patientDao.save(patient);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Patient> findAll() {
        return patientDao.findAll();
    }

    @Override
    public void delete(Patient patient) {
        patientDao.delete(patient);
    }
}
