package com.haulmont.testtask.core.service.doctor;

import com.haulmont.testtask.persistance.entity.Doctor;
import com.haulmont.testtask.core.service.CrudService;

import java.util.List;

/**
 * @author Alex Ilyushkin
 */
public interface DoctorService extends CrudService<Doctor> {

    List<Doctor> findAllWithPrescriptions();
}
