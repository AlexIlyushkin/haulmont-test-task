package com.haulmont.testtask.core.service.prescription;

import com.haulmont.testtask.persistance.entity.Doctor;
import com.haulmont.testtask.persistance.entity.Patient;
import com.haulmont.testtask.persistance.entity.Prescription;
import com.haulmont.testtask.core.service.CrudService;

import java.util.List;

/**
 * @author Alex Ilyushkin
 */
public interface PrescriptionService extends CrudService<Prescription> {

    List<Prescription> findAllWithDoctors();

    List<Prescription> findAllWithPatients();

    List<Prescription> findAllWithDoctorsAndPatients();

    List<Prescription> findPrescriptionBy(Doctor doctor,
                                          Patient patient,
                                          String description,
                                          Prescription.Priority priority);
}
