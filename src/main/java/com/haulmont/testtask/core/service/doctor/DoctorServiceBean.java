package com.haulmont.testtask.core.service.doctor;

import com.haulmont.testtask.persistance.dao.DoctorDao;
import com.haulmont.testtask.persistance.entity.Doctor;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Alex Ilyushkin
 */
@Service("doctorService")
@Transactional
public class DoctorServiceBean implements DoctorService {

    @Resource(name = "doctorDao")
    private DoctorDao doctorDao;

    @Override
    public Doctor save(Doctor doctor) {
        return doctorDao.save(doctor);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Doctor> findAll() {
        return doctorDao.findAll();
    }

    @Override
    public void delete(Doctor doctor) {
        doctorDao.delete(doctor);
    }

    @Override
    public List<Doctor> findAllWithPrescriptions() {
        return doctorDao.findAllWithPrescriptions();
    }
}
