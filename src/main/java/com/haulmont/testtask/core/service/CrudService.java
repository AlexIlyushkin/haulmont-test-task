package com.haulmont.testtask.core.service;

import java.util.List;

/**
 * @author Alex Ilyushkin
 */
public interface CrudService<T> {

    T save(T entity);

    List<T> findAll();

    void delete(T entity);
}
