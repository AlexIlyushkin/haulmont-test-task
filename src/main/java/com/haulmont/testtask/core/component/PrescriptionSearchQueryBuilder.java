package com.haulmont.testtask.core.component;

import com.haulmont.testtask.persistance.entity.Doctor;
import com.haulmont.testtask.persistance.entity.Patient;
import com.haulmont.testtask.persistance.entity.Prescription;

import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Objects;

/**
 * @author Alex Ilyushkin
 */
@Component
public class PrescriptionSearchQueryBuilder {

    public CriteriaQuery<Prescription> buildSearchQuery(CriteriaBuilder cb,
                                                        Doctor doctor,
                                                        Patient patient,
                                                        String description,
                                                        Prescription.Priority priority) {
        CriteriaQuery<Prescription> query = cb.createQuery(Prescription.class);
        Root<Prescription> root = query.from(Prescription.class);
        Predicate criteria = cb.conjunction();
        if (Objects.nonNull(doctor)) {
            criteria = cb.and(criteria, cb.equal(root.get("doctor"), doctor));
        }
        if (Objects.nonNull(patient)) {
            criteria = cb.and(criteria, cb.equal(root.get("patient"), patient));
        }
        if (Objects.nonNull(description) && description.length() > 0) {
            criteria = cb.and(criteria, cb.like(root.get("description"), '%' + description + '%'));
        }
        if (Objects.nonNull(priority)) {
            criteria = cb.and(criteria, cb.equal(root.get("priority"), priority));
        }
        return query.where(criteria);
    }

}
