package com.haulmont.testtask.ui.view.starterview;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;

/**
 * @author Alex Ilyushkin
 */
@SpringView
public class StarterView extends StarterViewDesign implements View {
}
