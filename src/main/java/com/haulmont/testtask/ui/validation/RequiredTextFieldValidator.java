package com.haulmont.testtask.ui.validation;

import com.vaadin.data.ValidationResult;
import com.vaadin.data.Validator;
import com.vaadin.data.ValueContext;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;

/**
 * @author Alex Ilyushkin
 */
@SpringComponent
@ViewScope
public class RequiredTextFieldValidator implements Validator<String> {

    private static final String ERROR_MESSAGE = "This field is required";

    @Override
    public ValidationResult apply(String s, ValueContext valueContext) {
        return s.length() != 0 ? ValidationResult.ok() : ValidationResult.error(ERROR_MESSAGE);
    }
}
