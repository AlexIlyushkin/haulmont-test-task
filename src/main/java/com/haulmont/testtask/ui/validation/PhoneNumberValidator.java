package com.haulmont.testtask.ui.validation;

import com.vaadin.data.ValidationResult;
import com.vaadin.data.Validator;
import com.vaadin.data.ValueContext;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;

import java.util.regex.Pattern;

/**
 * @author Alex Ilyushkin
 */
@SpringComponent
@ViewScope
public class PhoneNumberValidator implements Validator<String> {

    private static final String PHONE_NUMBER_REGEX_FORMAT = "^\\+?[\\d][-(]?\\d{3}[-)]?-?\\d{3}-?\\d{2}-?\\d{2}$";
    private static final String ERROR_MESSAGE = "Wrong phone number format";

    private Pattern pattern;

    public PhoneNumberValidator() {
        pattern = Pattern.compile(PHONE_NUMBER_REGEX_FORMAT);
    }

    @Override
    public ValidationResult apply(String s, ValueContext valueContext) {
        return pattern.matcher(s).matches() ? ValidationResult.ok() : ValidationResult.error(ERROR_MESSAGE);
    }
}
