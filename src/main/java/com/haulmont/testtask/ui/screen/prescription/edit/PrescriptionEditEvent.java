package com.haulmont.testtask.ui.screen.prescription.edit;

import com.haulmont.testtask.persistance.entity.Prescription;

/**
 * @author Alex Ilyushkin
 */
public class PrescriptionEditEvent {

    private Prescription editedPrescription;

    public PrescriptionEditEvent(Prescription editedPrescription) {
        this.editedPrescription = editedPrescription;
    }

    public Prescription getEditedPrescription() {
        return editedPrescription;
    }
}
