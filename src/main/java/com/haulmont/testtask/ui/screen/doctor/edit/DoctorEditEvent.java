package com.haulmont.testtask.ui.screen.doctor.edit;

import com.haulmont.testtask.persistance.entity.Doctor;

/**
 * @author Alex Ilyushkin
 */

public class DoctorEditEvent {

    private Doctor editedDoctor;

    public DoctorEditEvent(Doctor editedDoctor) {
        this.editedDoctor = editedDoctor;
    }

    public Doctor getEditedDoctor() {
        return editedDoctor;
    }
}
