package com.haulmont.testtask.ui.screen.patient.edit;

import com.vaadin.annotations.AutoGenerated;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.declarative.Design;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button;

/**
 * !! DO NOT EDIT THIS FILE !!
 * <p>
 * This class is generated by Vaadin Designer and will be overwritten.
 * <p>
 * Please make a subclass with logic and additional interfaces as needed,
 * e.g class LoginView extends LoginDesign implements View { }
 */
@DesignRoot
@AutoGenerated
@SuppressWarnings("serial")
public class PatientEditViewDesign extends VerticalLayout {
    protected TextField lastNameTextField;
    protected TextField firstNameTextField;
    protected TextField middleNameTextField;
    protected TextField phoneNumberTextField;
    protected Button okButton;
    protected Button cancelButton;

    public PatientEditViewDesign() {
        Design.read(this);
    }
}
