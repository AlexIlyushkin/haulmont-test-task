package com.haulmont.testtask.ui.screen.doctor.edit;

import com.haulmont.testtask.persistance.entity.Doctor;
import com.haulmont.testtask.ui.validation.RequiredTextFieldValidator;

import com.vaadin.data.Binder;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import org.vaadin.spring.events.EventBus.ViewEventBus;
import org.vaadin.viritin.form.AbstractForm;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @author Alex Ilyushkin
 */
@SpringComponent
@ViewScope
public class DoctorEditForm extends AbstractForm<Doctor> {

    private DoctorEditView doctorEditView;
    private ViewEventBus viewEventBus;
    private RequiredTextFieldValidator requiredTextFieldValidator;

    @Autowired
    public DoctorEditForm(DoctorEditView doctorEditView,
                          ViewEventBus viewEventBus,
                          RequiredTextFieldValidator requiredTextFieldValidator) {
        super(Doctor.class);
        setBinder(new Binder<>(Doctor.class));
        this.doctorEditView = doctorEditView;
        this.viewEventBus = viewEventBus;
        this.requiredTextFieldValidator = requiredTextFieldValidator;
    }

    @PostConstruct
    protected void init() {
        initBindings();
        initOkButtonClickEventListener();
        initCancelButtonClickEventListener();
    }

    private void initBindings() {
        initFirstNameBinding();
        initLastNameBinding();
        initMiddleNameBinding();
        initSpecializationBinding();
    }

    private void initFirstNameBinding() {
        getBinder()
                .forField(doctorEditView.firstNameTextField)
                .asRequired()
                .withValidator(requiredTextFieldValidator)
                .bind(Doctor::getFirstName, Doctor::setFirstName);
    }

    private void initLastNameBinding() {
        getBinder()
                .forField(doctorEditView.lastNameTextField)
                .asRequired()
                .withValidator(requiredTextFieldValidator)
                .bind(Doctor::getLastName, Doctor::setLastName);
    }

    private void initMiddleNameBinding() {
        getBinder()
                .forField(doctorEditView.middleNameTextField)
                .bind(Doctor::getMiddleName, Doctor::setMiddleName);
    }

    private void initSpecializationBinding() {
        getBinder()
                .forField(doctorEditView.specializationTextField)
                .asRequired()
                .withValidator(requiredTextFieldValidator)
                .bind(Doctor::getSpecialization, Doctor::setSpecialization);
    }


    private void initOkButtonClickEventListener() {
        doctorEditView.okButton.addClickListener(clickEvent -> {
            boolean valid = getBinder().writeBeanIfValid(getEntity());
            if (valid) {
                viewEventBus.publish(this, new DoctorEditEvent(getEntity()));
                UI.getCurrent().removeWindow(getPopup());
            }
        });
    }

    private void initCancelButtonClickEventListener() {
        doctorEditView.cancelButton.addClickListener(clickEvent -> {
            UI.getCurrent().removeWindow(getPopup());
        });
    }

    @Override
    public void setEntity(Doctor entity) {
        super.setEntity(entity);
        getBinder().setBean(entity);
    }

    @Override
    protected Component createContent() {
        return doctorEditView;
    }
}
