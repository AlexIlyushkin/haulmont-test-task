package com.haulmont.testtask.ui.screen.patient.browse;

import com.haulmont.testtask.persistance.entity.Patient;
import com.haulmont.testtask.core.service.patient.PatientService;
import com.haulmont.testtask.ui.screen.patient.edit.PatientEditEvent;
import com.haulmont.testtask.ui.screen.patient.edit.PatientEditForm;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaSystemException;
import org.vaadin.spring.events.EventBus.ViewEventBus;
import org.vaadin.spring.events.annotation.EventBusListenerMethod;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Optional;

/**
 * @author Alex Ilyushkin
 */
@SpringView
public class PatientBrowseView extends PatientBrowseViewDesign implements View {

    private static final String ADD_NEW_PATIENT_FORM_CAPTION = "Add new patient";
    private static final String EDIT_PATIENT_FORM_CAPTION = "Edit patient";

    private PatientEditForm patientEditForm;
    private ViewEventBus viewEventBus;
    private PatientService patientService;
    private Patient selectedPatient;

    @Autowired
    public PatientBrowseView(PatientEditForm patientEditForm, ViewEventBus viewEventBus, PatientService patientService) {
        this.patientEditForm = patientEditForm;
        this.viewEventBus = viewEventBus;
        this.patientService = patientService;
        viewEventBus.subscribe(this);
        patientsGrid.setItems(patientService.findAll());
    }

    @PostConstruct
    protected void init() {
        initGrid();
        initSelectionEventListener();
        initCreateButtonClickListener();
        initUpdateButtonClickListener();
        initDeleteButtonClickListener();
    }

    @PreDestroy
    protected void drop() {
        viewEventBus.unsubscribe(this);
    }

    @EventBusListenerMethod
    protected void onPatientEditEvent(PatientEditEvent patientEditEvent) {
        patientService.save(patientEditEvent.getEditablePatient());
        patientsGrid.setItems(patientService.findAll());
    }

    private void initGrid() {
        List<Patient> patients = patientService.findAll();
        patientsGrid.setItems(patients);
    }

    private void initSelectionEventListener() {
        patientsGrid.getSelectionModel().addSelectionListener(selectionEvent -> {
            Optional<Patient> selectedPatient = selectionEvent.getFirstSelectedItem();
            selectedPatient.ifPresent(patient -> {
                this.selectedPatient = patient;
                updateButton.setEnabled(true);
                deleteButton.setEnabled(true);
            });
        });
    }

    private void initCreateButtonClickListener() {
        createButton.addClickListener(clickEvent -> {
           patientEditForm.setEntity(new Patient());
           patientEditForm.openInModalPopup().setCaption(ADD_NEW_PATIENT_FORM_CAPTION);
        });
    }

    private void initUpdateButtonClickListener() {
        updateButton.addClickListener(clickEvent -> {
            patientEditForm.setEntity(selectedPatient);
            patientEditForm.openInModalPopup().setCaption(EDIT_PATIENT_FORM_CAPTION);
            updateButton.setEnabled(false);
            deleteButton.setEnabled(false);
        });
    }

    private void initDeleteButtonClickListener() {
        deleteButton.addClickListener(clickEvent -> {
            try {
                patientService.delete(selectedPatient);
                patientsGrid.setItems(patientService.findAll());
                updateButton.setEnabled(false);
                deleteButton.setEnabled(false);
            } catch (JpaSystemException e) {
                Notification.show("You cannot remove the patient because there is a prescription",
                        Notification.Type.WARNING_MESSAGE);
                e.printStackTrace();
            }
        });
    }
}
