package com.haulmont.testtask.ui.screen.prescription.edit;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;

/**
 * @author Alex Ilyushkin
 */
@SpringComponent
@ViewScope
public class PrescriptionEditView extends PrescriptionEditViewDesign {
}
