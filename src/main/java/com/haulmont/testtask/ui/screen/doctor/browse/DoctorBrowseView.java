package com.haulmont.testtask.ui.screen.doctor.browse;

import com.haulmont.testtask.persistance.entity.Doctor;
import com.haulmont.testtask.core.service.doctor.DoctorService;
import com.haulmont.testtask.ui.screen.doctor.edit.DoctorEditEvent;
import com.haulmont.testtask.ui.screen.doctor.edit.DoctorEditForm;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Notification;
import org.springframework.orm.jpa.JpaSystemException;
import org.vaadin.spring.events.EventBus.ViewEventBus;
import org.vaadin.spring.events.annotation.EventBusListenerMethod;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Optional;

/**
 * @author Alex Ilyushkin
 */
@SpringView
public class DoctorBrowseView extends DoctorBrowseViewDesign implements View {

    private static final String ADD_NEW_DOCTOR_FORM_CAPTION = "Add new doctor";
    private static final String EDIT_DOCTOR_FORM_CAPTION = "Edit doctor";
    private static final String STATISTICS_WINDOW_CAPTION = "Statistics";

    private DoctorService doctorService;
    private DoctorEditForm doctorEditForm;
    private Doctor selectedDoctor;
    private ViewEventBus viewEventBus;

    @Autowired
    public DoctorBrowseView(DoctorService doctorService,
                            DoctorEditForm doctorEditForm,
                            ViewEventBus viewEventBus) {
        this.doctorService = doctorService;
        this.doctorEditForm = doctorEditForm;
        this.viewEventBus = viewEventBus;
        viewEventBus.subscribe(this);
    }

    @PostConstruct
    protected void init() {
        initGrid();
        initSelectionListener();
        initCreateButtonClickListener();
        initUpdateButtonClickListener();
        initDeleteButtonClickListener();
    }

    @PreDestroy
    protected void drop() {
        viewEventBus.unsubscribe(this);
    }

    private void initGrid() {
        doctorsGrid.setItems(doctorService.findAll());
    }

    private void initSelectionListener() {
        doctorsGrid.getSelectionModel().addSelectionListener(selectionEvent -> {
            Optional<Doctor> firstSelected  = selectionEvent.getFirstSelectedItem();
            firstSelected.ifPresent(value -> {
                selectedDoctor = value;
                updateButton.setEnabled(true);
                deleteButton.setEnabled(true);
            });
        });
    }

    private void initCreateButtonClickListener() {
        createButton.addClickListener(clickEvent -> {
            doctorEditForm.setEntity(new Doctor());
            doctorEditForm.openInModalPopup().setCaption(ADD_NEW_DOCTOR_FORM_CAPTION);
        });
    }

    private void initUpdateButtonClickListener() {
        updateButton.addClickListener(clickEvent -> {
           doctorEditForm.setEntity(selectedDoctor);
           doctorEditForm.openInModalPopup().setCaption(EDIT_DOCTOR_FORM_CAPTION);
        });
    }

    private void initDeleteButtonClickListener() {
        deleteButton.addClickListener(clickEvent -> {
            try {
                doctorService.delete(selectedDoctor);
                doctorsGrid.setItems(doctorService.findAll());
                updateButton.setEnabled(false);
                deleteButton.setEnabled(false);
            } catch (JpaSystemException e) {
                Notification.show("You cannot remove the doctor because there is a prescription",
                        Notification.Type.WARNING_MESSAGE);
                e.printStackTrace();
            }
        });
    }

    @EventBusListenerMethod
    public void onDoctorEditEvent(DoctorEditEvent doctorEditEvent) {
        doctorService.save(doctorEditEvent.getEditedDoctor());
        doctorsGrid.setItems(doctorService.findAll());
        updateButton.setEnabled(false);
        deleteButton.setEnabled(false);
    }
}
