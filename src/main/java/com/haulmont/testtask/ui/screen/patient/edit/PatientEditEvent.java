package com.haulmont.testtask.ui.screen.patient.edit;

import com.haulmont.testtask.persistance.entity.Patient;

/**
 * @author Alex Ilyushkin
 */
public class PatientEditEvent {

    private Patient editablePatient;

    public PatientEditEvent(Patient editablePatient) {
        this.editablePatient = editablePatient;
    }

    public Patient getEditablePatient() {
        return editablePatient;
    }
}
