package com.haulmont.testtask.ui.screen.prescription.edit.validation;

import com.vaadin.data.HasValue;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.Validator;
import com.vaadin.data.ValueContext;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Alex Ilyushkin
 */
@SpringComponent
@ViewScope
public class ExpirationDateValidator implements Validator<LocalDate> {

    private static final String ERROR_MESSAGE = "Expiration date can't be before creation date";

    private HasValue<LocalDate> creationDateField;

    @Override
    public ValidationResult apply(LocalDate localDate, ValueContext valueContext) {
        if (Objects.nonNull(creationDateField)) {
            LocalDate expirationDate = creationDateField.getValue();
            if (expirationDate != null) {
                return localDate.isAfter(expirationDate) ? ValidationResult.ok()
                        : ValidationResult.error(ERROR_MESSAGE);
            }
            return ValidationResult.ok();
        }
        return ValidationResult.error("");
    }

    public void setCreationDateField(HasValue<LocalDate> creationDateField) {
        this.creationDateField = creationDateField;
    }
}
