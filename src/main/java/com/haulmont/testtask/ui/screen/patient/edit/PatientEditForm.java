package com.haulmont.testtask.ui.screen.patient.edit;

import com.haulmont.testtask.persistance.entity.Patient;
import com.haulmont.testtask.ui.validation.PhoneNumberValidator;
import com.haulmont.testtask.ui.validation.RequiredTextFieldValidator;

import com.vaadin.data.Binder;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import org.vaadin.spring.events.EventBus.ViewEventBus;
import org.vaadin.viritin.form.AbstractForm;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @author Alex Ilyushkin
 */
@SpringComponent
@ViewScope
public class PatientEditForm extends AbstractForm<Patient> {

    private PatientEditView patientEditView;
    private ViewEventBus viewEventBus;
    private RequiredTextFieldValidator requiredTextFieldValidator;
    private PhoneNumberValidator phoneNumberValidator;

    @Autowired
    public PatientEditForm(PatientEditView patientEditView,
                           ViewEventBus viewEventBus,
                           RequiredTextFieldValidator requiredTextFieldValidator,
                           PhoneNumberValidator phoneNumberValidator) {
        super(Patient.class);
        setBinder(new Binder<>());
        this.patientEditView = patientEditView;
        this.viewEventBus = viewEventBus;
        this.requiredTextFieldValidator = requiredTextFieldValidator;
        this.phoneNumberValidator = phoneNumberValidator;
    }

    @PostConstruct
    public void init() {
        initBindings();
        initOKButtonClickListener();
        initCancelButtonClickListener();
    }

    private void initBindings() {
        initFirstNameBinding();
        initLastNameBinding();
        initMiddleNameBinding();
        initPhoneNumberBinding();
    }

    private void initFirstNameBinding() {
        getBinder()
                .forField(patientEditView.firstNameTextField)
                .asRequired()
                .withValidator(requiredTextFieldValidator)
                .bind(Patient::getFirstName, Patient::setFirstName);
    }

    private void initLastNameBinding() {
        getBinder()
                .forField(patientEditView.lastNameTextField)
                .withValidator(requiredTextFieldValidator)
                .asRequired()
                .bind(Patient::getLastName, Patient::setLastName);
    }

    private void initMiddleNameBinding() {
        getBinder()
                .forField(patientEditView.middleNameTextField)
                .bind(Patient::getMiddleName, Patient::setMiddleName);
    }

    private void initPhoneNumberBinding() {
        getBinder()
                .forField(patientEditView.phoneNumberTextField)
                .asRequired()
                .withValidator(phoneNumberValidator)
                .bind(Patient::getPhoneNumber, Patient::setPhoneNumber);
    }

    private void initOKButtonClickListener() {
        patientEditView.okButton.addClickListener(clickEvent -> {
            boolean valid = getBinder().writeBeanIfValid(getEntity());
            if (valid) {
                viewEventBus.publish(this, new PatientEditEvent(getEntity()));
                UI.getCurrent().removeWindow(getPopup());
            }
        });
    }

    private void initCancelButtonClickListener() {
        patientEditView.cancelButton.addClickListener(clickEvent -> {
            UI.getCurrent().removeWindow(getPopup());
        });
    }

    @Override
    public void setEntity(Patient entity) {
        super.setEntity(entity);
        getBinder().setBean(entity);
    }

    @Override
    protected Component createContent() {
        return patientEditView;
    }
}
