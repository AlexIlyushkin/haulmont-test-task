package com.haulmont.testtask.ui.screen.doctor.edit;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;

/**
 * @author Alex Ilyushkin
 */
@SpringView
public class DoctorEditView extends DoctorEditViewDesign implements View {
}
