package com.haulmont.testtask.ui.screen.prescription.edit;

import com.haulmont.testtask.persistance.entity.Doctor;
import com.haulmont.testtask.persistance.entity.Patient;
import com.haulmont.testtask.persistance.entity.Prescription;
import com.haulmont.testtask.core.service.doctor.DoctorService;
import com.haulmont.testtask.core.service.patient.PatientService;
import com.haulmont.testtask.ui.convertation.LocalDateToDateConverter;

import com.haulmont.testtask.ui.screen.prescription.edit.validation.CreationDateValidator;
import com.haulmont.testtask.ui.screen.prescription.edit.validation.ExpirationDateValidator;
import com.vaadin.data.Binder;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import org.vaadin.spring.events.EventBus.ViewEventBus;
import org.vaadin.viritin.form.AbstractForm;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 * @author Alex Ilyushkin
 */
@SpringComponent
@ViewScope
public class PrescriptionEditForm extends AbstractForm<Prescription> {

    private PrescriptionEditView prescriptionEditView;
    private ViewEventBus viewEventBus;
    private LocalDateToDateConverter localDateToDateConverter;
    private DoctorService doctorService;
    private PatientService patientService;
    private CreationDateValidator creationDateValidator;
    private ExpirationDateValidator expirationDateValidator;

    @Autowired
    public PrescriptionEditForm(PrescriptionEditView prescriptionEditView,
                                ViewEventBus viewEventBus,
                                LocalDateToDateConverter localDateToDateConverter,
                                DoctorService doctorService,
                                PatientService patientService,
                                CreationDateValidator creationDateValidator,
                                ExpirationDateValidator expirationDateValidator) {
        super(Prescription.class);
        setBinder(new Binder<>());
        this.prescriptionEditView = prescriptionEditView;
        this.viewEventBus = viewEventBus;
        this.localDateToDateConverter = localDateToDateConverter;
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.creationDateValidator = creationDateValidator;
        this.expirationDateValidator = expirationDateValidator;
    }

    @PostConstruct
    protected void init() {
        initBindings();
        initOkButtonClickListener();
        initCancelButtonClickListener();
        initDoctorComboBox();
        initPatientComboBox();
        initPriorityComboBox();
    }

    private void initBindings() {
        initPatientBinding();
        initDoctorBinding();
        initCreationDateBinding();
        initExpirationDateBinding();
        initDescriptionBinding();
        initPriorityBinding();
    }

    private void initDescriptionBinding() {
        getBinder()
                .forField(prescriptionEditView.descriptionTextArea)
                .bind(Prescription::getDescription, Prescription::setDescription);
    }

    private void initPatientBinding() {
        getBinder()
                .forField(prescriptionEditView.patientComboBox)
                .asRequired()
                .bind(Prescription::getPatient, Prescription::setPatient);
    }

    private void initDoctorBinding() {
        getBinder()
                .forField(prescriptionEditView.doctorCompoBox)
                .asRequired()
                .bind(Prescription::getDoctor, Prescription::setDoctor);
    }

    private void initCreationDateBinding() {
        creationDateValidator.setExpirationDateField(prescriptionEditView.expirationDateField);
        getBinder()
                .forField(prescriptionEditView.creationDateField)
                .withValidator(creationDateValidator)
                .asRequired()
                .withConverter(localDateToDateConverter)
                .bind(Prescription::getCreationDate, Prescription::setCreationDate);
    }

    private void initExpirationDateBinding() {
        expirationDateValidator.setCreationDateField(prescriptionEditView.creationDateField);
        getBinder()
                .forField(prescriptionEditView.expirationDateField)
                .withValidator(expirationDateValidator)
                .asRequired()
                .withConverter(localDateToDateConverter)
                .bind(Prescription::getExpirationDate, Prescription::setExpirationDate);
    }

    private void initPriorityBinding() {
        getBinder()
                .forField(prescriptionEditView.priorityComboBox)
                .asRequired()
                .bind(Prescription::getPriority, Prescription::setPriority);
    }

    private void initOkButtonClickListener() {
        prescriptionEditView.okButton.addClickListener(clickEvent -> {
            boolean valid = getBinder().writeBeanIfValid(getEntity());
            if (valid) {
                viewEventBus.publish(this, new PrescriptionEditEvent(getEntity()));
                UI.getCurrent().removeWindow(getPopup());
            }
        });
    }

    private void initCancelButtonClickListener() {
        prescriptionEditView.cancelButton.addClickListener(clickEvent -> {
           UI.getCurrent().removeWindow(getPopup());
        });
    }

    private void initDoctorComboBox() {
        List<Doctor> doctors = doctorService.findAll();
        prescriptionEditView.doctorCompoBox.setItems(doctors);
    }

    private void initPatientComboBox() {
        List<Patient> patients = patientService.findAll();
        prescriptionEditView.patientComboBox.setItems(patients);
    }

    private void initPriorityComboBox() {
        List<Prescription.Priority> priorities = Arrays.asList(Prescription.Priority.values());
        prescriptionEditView.priorityComboBox.setItems(priorities);
    }

    private void initCreationDateChangeValueListener() {
        prescriptionEditView.creationDateField.addValueChangeListener(valueChangeEvent -> {
           valueChangeEvent.getComponent();
        });
    }

    @Override
    public void setEntity(Prescription entity) {
        super.setEntity(entity);
        getBinder().setBean(entity);
    }

    @Override
    protected Component createContent() {
        return prescriptionEditView;
    }
}
