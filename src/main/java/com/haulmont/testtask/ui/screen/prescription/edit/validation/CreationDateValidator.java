package com.haulmont.testtask.ui.screen.prescription.edit.validation;

import com.vaadin.data.HasValue;
import com.vaadin.data.ValidationResult;
import com.vaadin.data.Validator;
import com.vaadin.data.ValueContext;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;

import java.time.LocalDate;

/**
 * @author Alex Ilyushkin
 */
@SpringComponent
@ViewScope
public class CreationDateValidator implements Validator<LocalDate> {

    private static final String ERROR_MESSAGE = "Creation date can't be after expiration date";

    private HasValue<LocalDate> expirationDateField;

    @Override
    public ValidationResult apply(LocalDate date, ValueContext valueContext) {
        LocalDate expirationDate = expirationDateField.getValue();
        if (expirationDate != null) {
            return date.isBefore(expirationDate) ? ValidationResult.ok()
                    : ValidationResult.error(ERROR_MESSAGE);
        }
        return ValidationResult.ok();
    }

    public void setExpirationDateField(HasValue<LocalDate> expirationDateField) {
        this.expirationDateField = expirationDateField;
    }
}
