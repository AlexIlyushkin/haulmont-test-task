package com.haulmont.testtask.ui.screen.prescription.browse;

import com.haulmont.testtask.core.service.doctor.DoctorService;
import com.haulmont.testtask.core.service.patient.PatientService;
import com.haulmont.testtask.persistance.entity.Prescription;
import com.haulmont.testtask.core.service.prescription.PrescriptionService;
import com.haulmont.testtask.ui.screen.prescription.edit.PrescriptionEditEvent;
import com.haulmont.testtask.ui.screen.prescription.edit.PrescriptionEditForm;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.events.EventBus.ViewEventBus;
import org.vaadin.spring.events.annotation.EventBusListenerMethod;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Optional;

/**
 * @author Alex Ilyushkin
 */
@SpringView
public class PrescriptionBrowseView extends PrescriptionBrowseViewDesign implements View {

    private static final String ADD_NEW_PRESCRIPTION_CAPTION_FORM = "Add new prescription";
    private static final String EDIT_PRESCRIPTION_CAPTION_FORM = "Edit prescription";

    private PrescriptionEditForm prescriptionEditForm;
    private PrescriptionService prescriptionService;
    private DoctorService doctorService;
    private PatientService patientService;
    private ViewEventBus viewEventBus;
    private Prescription selectedPrescription;

    @Autowired
    public PrescriptionBrowseView(PrescriptionEditForm prescriptionEditForm,
                                  PrescriptionService prescriptionService,
                                  DoctorService doctorService,
                                  PatientService patientService,
                                  ViewEventBus viewEventBus) {
        this.prescriptionEditForm = prescriptionEditForm;
        this.prescriptionService = prescriptionService;
        this.viewEventBus = viewEventBus;
        this.doctorService = doctorService;
        this.patientService = patientService;
        viewEventBus.subscribe(this);
    }

    @PostConstruct
    public void init() {
        initGrid();
        initSelectionListener();
        initCreateButtonClickListener();
        initUpdateButtonClickListener();
        initDeleteButtonClickListener();
        initDoctorFilterComboBox();
        initPatientFilterComboBox();
        initConfirmFilterButtonClickListener();
        initPriorityFilterComboBox();
    }

    @PreDestroy
    public void drop() {
        viewEventBus.unsubscribe(this);
    }

    @EventBusListenerMethod
    protected void onPrescriptionEditEvent(PrescriptionEditEvent prescriptionEditEvent) {
        prescriptionService.save(prescriptionEditEvent.getEditedPrescription());
        prescriptionsGrid.setItems(prescriptionService.findAllWithDoctorsAndPatients());
    }

    private void initGrid() {
        List<Prescription> prescriptions = prescriptionService.findAllWithDoctorsAndPatients();
        prescriptionsGrid.setItems(prescriptions);
    }

    private void initSelectionListener() {
        prescriptionsGrid.getSelectionModel().addSelectionListener(selectionEvent -> {
           Optional<Prescription> selectedPrescription = selectionEvent.getFirstSelectedItem();
           selectedPrescription.ifPresent((value) -> {
               this.selectedPrescription = value;
               updateButton.setEnabled(true);
               deleteButton.setEnabled(true);
           });
        });
    }

    private void initCreateButtonClickListener() {
        createButton.addClickListener(clickEvent -> {
           prescriptionEditForm.setEntity(new Prescription());
           prescriptionEditForm.openInModalPopup().setCaption(ADD_NEW_PRESCRIPTION_CAPTION_FORM);
        });
    }

    private void initUpdateButtonClickListener() {
        updateButton.addClickListener(clickEvent -> {
            prescriptionEditForm.setEntity(selectedPrescription);
            prescriptionEditForm.openInModalPopup().setCaption(EDIT_PRESCRIPTION_CAPTION_FORM);
            updateButton.setEnabled(false);
            deleteButton.setEnabled(false);
        });
    }

    private void initDeleteButtonClickListener() {
        deleteButton.addClickListener(clickEvent -> {
           prescriptionService.delete(selectedPrescription);
           prescriptionsGrid.setItems(prescriptionService.findAllWithDoctorsAndPatients());
            updateButton.setEnabled(false);
            deleteButton.setEnabled(false);
        });
    }

    private void initDoctorFilterComboBox() {
        doctorFilterComboBox.setItems(doctorService.findAll());
    }

    private void initPatientFilterComboBox() {
        patientFilterComboBox.setItems(patientService.findAll());
    }

    private void initPriorityFilterComboBox() {
        priorityFilterComboBox.setItems(Prescription.Priority.values());
    }

    private void initConfirmFilterButtonClickListener() {
        confirmFilterButton.addClickListener(clickEvent -> {
           List<Prescription> prescriptions = prescriptionService.findPrescriptionBy(
                   doctorFilterComboBox.getValue(),
                   patientFilterComboBox.getValue(),
                   descriptionFilterTextField.getValue(),
                   priorityFilterComboBox.getValue());
           prescriptionsGrid.setItems(prescriptions);
        });
    }
}
