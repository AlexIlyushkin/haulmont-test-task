package com.haulmont.testtask.ui.convertation;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;

/**
 * @author Alex Ilyushkin
 */
@SpringComponent
@ViewScope
public class LocalDateToDateConverter implements Converter<LocalDate, Date> {

    @Override
    public Result<Date> convertToModel(LocalDate localDate, ValueContext valueContext) {
        Instant instant = localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
        Date date = Date.from(instant);
        return Result.ok(date);
    }

    @Override
    public LocalDate convertToPresentation(Date date, ValueContext valueContext) {
        if (Objects.nonNull(date)) {
            Date safeDate = new Date(date.getTime());
            return safeDate.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
        }
        return null;
    }


}
