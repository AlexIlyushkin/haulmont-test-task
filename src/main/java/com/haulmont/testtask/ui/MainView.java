package com.haulmont.testtask.ui;

import com.haulmont.testtask.ui.navigation.NavigationManager;
import com.haulmont.testtask.ui.screen.doctor.browse.DoctorBrowseView;
import com.haulmont.testtask.ui.screen.patient.browse.PatientBrowseView;
import com.haulmont.testtask.ui.screen.prescription.browse.PrescriptionBrowseView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @author Alex Ilyushkin
 */
@SpringViewDisplay
@UIScope
public class MainView extends MainViewDesign implements ViewDisplay {

    private NavigationManager navigationManager;

    public MainView(NavigationManager navigationManager) {
        this.navigationManager = navigationManager;
    }

    @PostConstruct
    protected void init() {
        bindNavigationButton(docotorViewNavigationButton, DoctorBrowseView.class);
        bindNavigationButton(patientViewNavigationButton, PatientBrowseView.class);
        bindNavigationButton(prescriptionViewNavigationButton, PrescriptionBrowseView.class);
    }

    private void bindNavigationButton(Button button, Class<? extends View> viewClass) {
        button.addClickListener(clickEvent -> navigationManager.navigateTo(viewClass));
    }

    @Override
    public void showView(View view) {
        viewHolder.removeAllComponents();
        viewHolder.addComponents(view.getViewComponent());
    }
}
