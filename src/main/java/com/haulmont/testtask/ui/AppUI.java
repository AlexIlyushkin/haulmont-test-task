package com.haulmont.testtask.ui;

import com.haulmont.testtask.ui.navigation.NavigationManager;
import com.haulmont.testtask.ui.view.starterview.StarterView;

import com.vaadin.annotations.Title;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Alex Ilyushkin
 */
@Title("Haulmont Test Task")
@SpringUI
public class AppUI extends UI {

    private MainView mainView;
    private NavigationManager navigationManager;

    @Autowired
    public AppUI(MainView mainView, NavigationManager navigationManager) {
        this.mainView = mainView;
        this.navigationManager = navigationManager;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setContent(mainView);
        navigationManager.navigateTo(StarterView.class);
    }
}
