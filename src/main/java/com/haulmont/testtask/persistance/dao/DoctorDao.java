package com.haulmont.testtask.persistance.dao;

import com.haulmont.testtask.persistance.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Alex Ilyushkin
 */
@Repository
public interface DoctorDao extends JpaRepository<Doctor, Long> {

    @Query("select distinct d from Doctor d join fetch d.prescriptions")
    List<Doctor> findAllWithPrescriptions();
}
