package com.haulmont.testtask.persistance.dao;

import com.haulmont.testtask.persistance.entity.Doctor;
import com.haulmont.testtask.persistance.entity.Patient;
import com.haulmont.testtask.persistance.entity.Prescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Alex Ilyushkin
 */
@Repository
public interface PrescriptionDao extends JpaRepository<Prescription, Long> {

    @Query("select distinct p from Prescription p join fetch p.doctor")
    List<Prescription> findAllWithDoctors();

    @Query("select distinct p from Prescription p join fetch p.patient")
    List<Prescription> findAllWithPatients();

    @Query("select distinct p from Prescription p join fetch p.doctor join fetch p.patient")
    List<Prescription> findAllWithDoctorsAndPatients();

    @Query("select p from Prescription p join fetch p.doctor join fetch p.patient where p.doctor = :doctor and " +
            "p.patient = :patient and " +
            "p.description like concat('%', :description, '%') ")
    List<Prescription> findPrescriptionsBy(Doctor doctor, Patient patient, String description);
}
