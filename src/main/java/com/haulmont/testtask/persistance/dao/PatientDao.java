package com.haulmont.testtask.persistance.dao;

import com.haulmont.testtask.persistance.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Alex Ilyushkin
 */
@Repository
public interface PatientDao extends JpaRepository<Patient, Long> {
}
