package com.haulmont.testtask.persistance.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author Alex Ilyushkin
 */
@Entity(name = "Doctor")
@Table(name = "DOCTOR")
public class Doctor extends AbstractPerson implements Serializable {

    @Column(name = "specialization")
    private String specialization;

    @OneToMany(targetEntity = Prescription.class,
            cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY)
    private List<Prescription> prescriptions;

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public List<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(List<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }

    @Override
    public String toString() {
        return getLastName() + " " +
                getFirstName() + " " +
                (getMiddleName() != null ? getMiddleName() : "") + ", " +
                getSpecialization();
    }
}
