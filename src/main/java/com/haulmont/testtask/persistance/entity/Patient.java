package com.haulmont.testtask.persistance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Alex Ilyushkin
 */
@Entity(name = "Patient")
@Table(name = "PATIENT")
public class Patient extends AbstractPerson implements Serializable {

    @Column(name = "phone_number")
    private String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return getLastName() + " " +
                getFirstName() +
                (Objects.nonNull(getMiddleName()) ? " "  + getMiddleName() : "") + ", " +
                getPhoneNumber();
    }
}
